import pika
import time
import subprocess

# Configura la connessione a RabbitMQ
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

# Dichiarazione della coda
channel.queue_declare(queue='access_log_queue')

while True:
    # Esegui il comando "who" per ottenere gli utenti connessi
    who_output = subprocess.check_output(["who"]).decode("utf-8")
    
    # Invia i dati degli utenti connessi alla coda RabbitMQ
    channel.basic_publish(exchange='', routing_key='access_log_queue', body=who_output)
    print("[x] Inviato dati sugli utenti connessi.")
    
    # Attendere un certo intervallo di tempo prima di eseguire di nuovo il comando
    time.sleep(60)  # Esegui ogni minuto

# Chiudi la connessione (questo codice non verrà mai eseguito poiché il ciclo è infinito)
connection.close()
