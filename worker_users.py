import pika

# Funzione di callback per la gestione dei dati sugli utenti
def callback(ch, method, properties, body):
    data = body.decode()  # Converte i dati da bytes a stringa
    # Registra i dati sugli utenti in un file di registro
    with open('user_access_log.txt', 'a') as log_file:
        log_file.write(data)
    
    print("[x] Ricevuti dati sugli utenti connessi.")

# Configura la connessione a RabbitMQ
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

# Dichiarazione della coda
channel.queue_declare(queue='access_log_queue')

# Configura la funzione di callback per i messaggi
channel.basic_consume(queue='access_log_queue', on_message_callback=callback, auto_ack=True)

print(' [*] In attesa di dati sugli utenti connessi. Premi CTRL+C per uscire.')

# Inizia ad ascoltare la coda
channel.start_consuming()
